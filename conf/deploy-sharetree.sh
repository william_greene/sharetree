#!/bin/bash
cd /var/www
TODAY=`date +%Y-%m-%d-%H-%M-%S`
DEPLOYDIR="/var/www/deploy/sharetree-$TODAY"
cp -r /home/ubuntu/sharetree $DEPLOYDIR
rm current-sharetree
ln -s $DEPLOYDIR current-sharetree