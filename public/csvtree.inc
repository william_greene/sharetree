<?php

//////////// nodeinfo

// special nodes
$nodeInfo = array(
	'_root' => array(
		'best_name' => 'Twitter Drives 4x<br> as much traffic<br> as you think',
		'icon' => '/img/screenshot.png',
		'type' => 'rootNode'
	),
	'_blank' => array(
		'best_name' => 'Direct',
		'icon' => '/img/crowd.png',
		'type' => 'directNode',
		'parent_id' => '_root'
	)
);

// domain nodes
$domainsJson = file_get_contents('data/domains.json');
$domains = json_decode($domainsJson,true);
foreach($domains as $domain => $info) {
	// skip unimpressive referrers
	if ($info['pv_count'] < 20) continue;
	
	$info['parent_id'] = '_root';
	$info['type'] = 'domain';
	if (empty($info['icon'])) {
		$info['icon'] = 'http://' . $info['domain'] . '/favicon.ico';
	}
	$nodeInfo[$domain] = $info;
}


// get all the awesm url tweets (i.e. first tweets), and mentions counts
// index them by awesm_id, so we can attach them when those awesm_urls turn up
$firstTweets = array();
$statusIdToAwesmUrlMap = array();
if (($handle = fopen("data/awesmurl-tweets.csv", "r")) !== FALSE) {
    while (($data = fgetcsv($handle, 1000, ",")) !== FALSE) {
        $awesmId = $data[0];
		$firstTweets[$awesmId] = array(
			'icon' => $data[5],
			'username' => $data[3],
			'full_name' => $data[4],
			'tweet_url' => $data[6],
			'tweet_body' => $data[7],
			'timestamp' => $data[2],
			'date' => strftime('%e %B',$data[2]),
			'tool' => $data[11],
			'status_id' => $data[8],
			'mentions' => $data[1]
		);
		$statusIdToAwesmUrlMap[$data[8]] = $awesmId;
		error_log("Status id " . $data[8]);
    }
    fclose($handle);
}

// awesm url nodes
$awesmUrlsJson = file_get_contents('data/awesmurls.json');
$awesmUrls = json_decode($awesmUrlsJson,true);
foreach($awesmUrls as $awesmUrl => $info) {
	if (array_key_exists($awesmUrl,$firstTweets)) {
		// import metadata from the tweets sheet
		foreach($firstTweets[$awesmUrl] as $key => $value) {
			$info[$key] = $value;
		}
		$info['type'] = 'tweet';
		$nodeInfo[$awesmUrl] = $info;
		error_log("Tweet found for $awesmUrl");
	}
	else
	{
		//error_log("No first tweet found for $awesmUrl");
	}
}

//$data[7] == original status id

// mentions nodes
// skip tweets that are already attached to shares (first tweets)
if (($handle = fopen("data/tweets-20110823.csv", "r")) !== FALSE) {
    while (($data = fgetcsv($handle, 1000, ",")) !== FALSE) {
        $statusId = 's'.$data[9];
		$parent = $data[14];

        if($_REQUEST['rt_chains'] != 'no') {
			$parentStatusId = $data[17];
            if (!empty($parentStatusId)) {
				if (array_key_exists($parentStatusId,$statusIdToAwesmUrlMap)) {
					$parent = $statusIdToAwesmUrlMap[$parentStatusId];
				} else {
					$parent = 's'.$data[17];
				}
                error_log("$statusId is child of parent $parent");
            }
        }
		if (empty($parent)) continue;
		// skip first tweets:
		if ($firstTweets[$parent]['status_id'] == $data[9]) {
            $awesmUrl = $data[14];
            $nodeInfo[$awesmUrl]['tool'] = $data[16];
            $nodeInfo[$awesmUrl]['follower_count'] = $data[15];
            continue;
        }
		//if($data[17] == 'original') continue;
		
		$info = array(
			'type' => 'mention',
			'icon' => $data[4],
			'username' => $data[2],
			'full_name' => $data[3],
			'tweet_url' => $data[5],
			'tweet_body' => $data[6],
			'timestamp' => $data[0],
			'date' => strftime('%e %B',$data[0]),
			'tool' => $data[16],
			'retweet_or_mention' => $data[19],
			'parent_id' => $parent,
			'follower_count' => $data[15]
		);
		$nodeInfo[$statusId] = $info;
    }
    fclose($handle);
}

///////////// map

$map = array();
foreach($nodeInfo as $childId => $node)
{
	// skip suspicious-looking children here
	
	$map[] = array(
		'parent_id' => $node['parent_id'],
		'child_id' => $childId
	);
}

//error_log(print_r(array_slice($map,0,100),true));