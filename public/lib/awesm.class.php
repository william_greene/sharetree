<?php

class Awesm {

	private $apiKey;
	private $createHost = 'http://api.awe.sm';
	private $statsHost = 'http://api.awe.sm';
	private $apiVersion = 2;
	private $contentType;

	const EP_CREATE = '/url.';
	const EP_CREATE_STATIC = '/url/static.json';
	const EP_CREATE_AND_REDIRECT = '/url/share';
	const EP_STATS_CLICKS = '/clicks.json';
	const EP_STATS_SHARES = '/shares';
	const EP_STATS_ACTIVITY = '/activity';
	const EP_STATS_CLICKS_LIST = '/clicks/list.json';
	const EP_STATS_SHARES_LIST = '/shares/list';
	const EP_STATS_CLICKS_DETAILS = '/clicks/';


	const DIM_CAMPAIGN = 'campaign';
	const DIM_CHANNEL = 'channel';
	const DIM_TOOL = 'tool';
	const DIM_USER = 'user_id';
	const DIM_NOTES = 'notes';
	const DIM_PARENT = 'parent_awesm';
	const DIM_SHARER = 'sharer_id';

	public function __construct($apiKey, $defaults=null) {
		$this->apiKey = $apiKey;
		if (!is_null($defaults)) {
			if (array_key_exists('create_host', $defaults)) {
				$this->createHost = $defaults['create_host'];
			}
			if (array_key_exists('stats_host', $defaults)) {
				$this->statsHost = $defaults['stats_host'];
			}
			if (array_key_exists('api_version', $defaults)) {
				$this->apiVersion = $defaults['api_version'];
			}
			// TODO: etc.
		}
	}

	public function getContentType() {
		return $this->contentType;
	}

	public function getClickStats(
	$after=null, $before=null, $dimensions=null, $interval=null, $groupBy=null, $page=null, $perPage=null, $offset=null, $pivot=null, $withZeros=null, $withMetadata=null, $callback=null
	) {
		$params = array();
		if (!empty($before))
			$params['clicked_before'] = $before;
		if (!empty($after))
			$params['clicked_after'] = $after;
		if (is_array($dimensions)) {
			foreach ($dimensions as $dimension => $value) {
				$params[$dimension] = $value;
			}
		}
		if (!empty($interval))
			$params['interval'] = $interval;
		if (!empty($groupBy))
			$params['group_by'] = $groupBy;
		if (!empty($page))
			$params['page'] = $page;
		if (!empty($perPage))
			$params['per_page'] = $perPage;
		if ($offset !== null)
			$params['offset'] = $offset;
		if (!empty($pivot))
			$params['pivot'] = $pivot;
		if ($withZeros !== null)
			$params['with_zeros'] = $withZeros;
		if (!empty($callback))
			$params['callback'] = $callback;
		if (!empty($withMetadata))
			$params['with_metadata'] = 'true';

		return $this->fetchStats(self::EP_STATS_CLICKS, $params);
	}

	public function getClickDetails(
	$domain, $stub, $after=null, $before=null, $interval=null, $page=null, $perPage=null, $offset=null, $withZeros=null, $callback=null
	) {

		$params = array();
		if (!empty($before))
			$params['clicked_before'] = $before;
		if (!empty($after))
			$params['clicked_after'] = $after;
		if (!empty($interval))
			$params['interval'] = $interval;
		if (!empty($page))
			$params['page'] = $page;
		if (!empty($perPage))
			$params['per_page'] = $perPage;
		if ($offset !== null)
			$params['offset'] = $offset;
		if ($withZeros !== null)
			$params['with_zeros'] = $withZeros;
		if (!empty($callback))
			$params['callback'] = $callback;

		$awesm_url = "{$domain}_{$stub}";
		return $this->fetchStats(self::EP_STATS_CLICKS_DETAILS . $awesm_url . ".json", $params);
	}

	public function getClicksList(
	$shareList=null, $after=null, $before=null, $interval=null, $totals=false, $page=null, $perPage=null, $offset=null, $withZeros=null, $withMetadata=null, $callback=null
	) {
		$params = array();
		if (!empty($shareList)) {
			$shareValues = implode(",", $shareList);
			$params['share_list'] = $shareValues;
		}
		if (!empty($before))
			$params['clicked_before'] = $before;
		if (!empty($after))
			$params['clicked_after'] = $after;
		if (!empty($interval))
			$params['interval'] = $interval;
		//FAIL: this is the reverse of what it should be, it is a current bug
		if (!empty($totals))
			$params['with_totals'] = 'true';
		if (!empty($page))
			$params['page'] = $page;
		if (!empty($perPage))
			$params['per_page'] = $perPage;
		if ($offset !== null)
			$params['offset'] = $offset;
		if ($withZeros !== null)
			$params['with_zeros'] = $withZeros;
		if ($withMetadata === true)
			$params['with_metadata'] = 'true';
		if (!empty($callback))
			$params['callback'] = $callback;

		return $this->fetchStats(self::EP_STATS_CLICKS_LIST, $params);
	}

	public function getShareStats(
	$after=null, $before=null, $dimensions=null, $format='.json', $interval=null, $callback=null, $groupBy=null
	) {
		$params = array();
		if (!empty($before))
			$params['created_before'] = $before;
		if (!empty($after))
			$params['created_after'] = $after;
		if (!empty($interval))
			$params['interval'] = $interval;
		if (!empty($groupBy))
			$params['group_by'] = $groupBy;
		if (!empty($callback))
			$params['callback'] = $callback;

		if (is_array($dimensions)) {
			foreach ($dimensions as $dimension => $value) {
				$params[$dimension] = $value;
			}
		}
		return $this->fetchStats(self::EP_STATS_SHARES . $format, $params);
	}

	public function getSharesList(
	$after=null, $before=null, $dimensions=null, $format='.json', $callback=null
	) {
		$params = array();
		if (!empty($before))
			$params['created_before'] = $before;
		if (!empty($after))
			$params['created_after'] = $after;
		if (!empty($callback))
			$params['callback'] = $callback;
		if (is_array($dimensions)) {
			foreach ($dimensions as $dimension => $value) {
				$params[$dimension] = $value;
			}
		}
		return $this->fetchStats(self::EP_STATS_SHARES_LIST . $format, $params);
	}

	public function v1_getInfoPublic(
	$stub, $domain=null, $startDate=null, $endDate=null, $format='json') {
		return $this->v1_getInfo($stub, $domain, $startDate, $endDate, $format, false);
	}

	public function v1_getInfo(
	$stub, $domain=null, $startDate=null, $endDate=null, $format='json', $bApiKey=true) {
		$params = array();
		if (!empty($domain)) {
			$params['domain'] = $domain;
		}

		$params['start_date'] = $startDate;
		$params['end_date'] = $endDate;

		$url = '/url/' . $stub . '.' . $format;

		return $this->fetchStats($url, $params, $format, $bApiKey);
	}

	public function v1_lookup(
	$start=null, $end=null, $createdStart=null, $createdEnd=null, $domain=null, $originalUrl=null, $shareType=null, $createType=null, $userId=null, $notes=null, $parentAwesm=null, $sharerId=null, $clicked=null, $details=false, $page=null, $perPage=null, $format='json', $bApiKey=true
	) {

		$url = '/url.' . $format;
		$params = array(
				'start_date' => $start,
				'end_date' => $end,
				'created_at_start' => $createdStart,
				'created_at_end' => $createdEnd,
				'domain' => $domain,
				'original_url' => $originalUrl,
				'share_type' => $shareType,
				'create_type' => $createType,
				'user_id' => $userId,
				'notes' => $notes,
				'parent_awesm' => $parentAwesm,
				'sharer_id' => $sharerId,
				'clicked' => $clicked,
				'details' => $details,
				'page' => $page,
				'per_page' => $perPage
		);

		return $this->fetchStats($url, $params, $format, $bApiKey);
	}

	/**
	 * Call a path on the stats host
	 * @param string $path Relative; you must pass a leading slash.
	 */
	private function fetchStats($base, $params, $responseFormat='json', $bApiKey=true) {
		// insert implicit params
		$params['version'] = $this->apiVersion;
		if ($bApiKey)
			$params['api_key'] = $this->apiKey;

		// turn params into arguments
		$kvPairs = array();
		foreach ($params as $key => $value) {
			//if (!empty($value)) $kvPairs[] = urlencode($key). '=' . urlencode($value);
			if ($value !== null)
				$kvPairs[] = urlencode($key) . '=' . urlencode($value);
		}

		// combine arguments with endpoint
		$path = '?' . implode('&', $kvPairs);
		$url = $this->statsHost . $base . $path;
		error_log("Fetching stats: $url");

		// call server
		$ch = curl_init($url);
		curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
		curl_setopt($ch, CURLOPT_HEADER, false);
		$response = curl_exec($ch);
		//error_log("Full response: " . $response);

		// fetch and save the response content type
		$this->contentType = curl_getinfo($ch, CURLINFO_CONTENT_TYPE);

		// parse response
		if ($responseFormat == 'json') {
			$result = json_decode($response, true);
			if (empty($result)) {
				$result = $response;
			}
		} else if ($responseFormat == 'xml') {
			$result = new SimpleXMLElement($response);
		} else {
			$result = "Unknown response format";
		}
		return $result;
	}

	/* @TODO: setters for defaults that will apply to all links created
	 * - format
	 * - version
	 * - share_type
	 * - create_type
	 * - parent_awesm
	 * - domain
	 * - sharer_id
	 * - user_id
	 * - notes
	 * - callback
	 */

	/**
	 * Creates a new awesm URL from an original URL
	 *
	 * @param unknown_type $originalUrl
	 * @param unknown_type $params
	 * @return AwesmUrl
	 */
	public function createStaticLink($originalUrl, $params=null) {
		// insert implicit params
		$params['v'] = $this->apiVersion;
		$params['key'] = $this->apiKey;
		$params['url'] = $originalUrl;
		$params['channel'] = 'static';

		// create query string
		$query = http_build_query($params);

		// combine arguments with endpoint
		$path = '?' . $query;
		$url = $this->createHost . self::EP_CREATE_STATIC . $path;
		error_log("Creating static URL: $url");

		// call server
		$ch = curl_init($url);
		curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
		curl_setopt($ch, CURLOPT_HEADER, false);
		$response = curl_exec($ch);
		//error_log("Full response: " . $response);

		$result = json_decode($response,true);
		return new AwesmUrl($result);
	}

	public function getActivityStats(
	$startDate=null, $endDate=null, $dimensions=null, $interval=null, $groupBy=null, $format='.json', $callback=null
	) {
		$params = array();
		$this->addIfNotEmpty('start_date', $startDate, $params);
		$this->addIfNotEmpty('end_date', $endDate, $params);
		$this->addIfNotEmpty('interval', $interval, $params);
		$this->addIfNotEmpty('group_by', $groupBy, $params);
		$this->addAllValues($dimensions, $params);
		$this->addIfNotEmpty('callback', $callback, $params);

		return $this->fetchStats(self::EP_STATS_ACTIVITY . $format, $params);
	}

	function addIfNotEmpty($key, $value, &$array) {
		if (!empty($value))
			$array[$key] = $value;
	}

	function addAllValues($inputArray, &$containerArray) {
		if (is_array($inputArray)) {
			foreach ($inputArray as $key => $value) {
				$containerArray[$key] = $value;
			}
		}
	}

}

/**
 * Stub class for Awesm URLs. Allows us to treat them as strings or objects.
 * TODO: should it be possible to create new AwesmUrl(<url>) and get link stats?
 * TODO: or should doing so use <url> as an original URL and create a new link?
 */
class AwesmUrl
{
	private $awesmUrl;
	private $data;

	public function __construct($urlData)
	{
		$this->data = $urlData['url'];
		$this->awesmUrl = $urlData['url']['awesm_url'];
	}

	public function getData()
	{
		return $this->data;
	}

	public function __toString()
	{
		return $this->awesmUrl;
	}
}
