<?php

class Visualization_Google extends Visualization {

	public function draw()
	{
		error_log(print_r($this->map, true));

		$vertices = array();
		foreach ($this->map as $node)
		{
			$vertices[] = $node['parent_id'] . '--' . $node['child_id'];
		}
		$graphString = implode(';', $vertices);
		error_log("Graph as string: $graphString");
		
		//$graphString = 'google.com--twitter.com,twitter.com--3,a--3,3--9423423423';
		//$graphString = 'root--blank;root--twittercom;root--techmemecom;root--techcrunchcom;root--googlecom;root--sphinncom;root--totallyawesm;root--hootsuitecom;root--facebookcom;root--tumblrcom;root--t3nde;root--reuterscom;root--summifycom;root--ycombinatorcom;root--mediagazercom;root--bothsidesofthetablecom;root--econsultancycom;root--tweetpost;root--linkedincom;root--businessinsidercom;root--deliciouscom;root--htly;root--discovermagazinecom;root--snowballfactorycom;root--visiblicom;root--iconfactorycom;root--yahoocom;root--webpronewscom;root--allthingsdcom;root--mailyahoonet;root--nextlevelofnewscom;root--netnewscheckcom;root--nextmediasourcecom;root--livecom;root--wordpresscom;root--tweetedtimescom;root--redditcom;root--paperli;root--googlecouk;root--emediavitalscom;80879991--80885129;80885129--80899869;80893958--80907723;81546865--81577837;80885129--80889078;80879991--80886415;80879347--81553940;80879991--80884373;80885129--80904200;77238120--77240711;81013811--81023191;81166656--81184529;80919410--81123183;81030419--81030942;80893958--80899142;80885129--80886335;81012612--81012985;81048474--81056904;80907723--81566634;81101950--81296658;76277650--80890157;techmemecom--80879991;root--80908922;root--80893958;root--81101950;techcrunchcom--81101132;root--81030419;techcrunchcom--81137444;techmemecom--81012612;totallyawesm--81155928;techcrunchcom--80892368;businessinsidercom--81342651;ycombinatorcom--80917418;yahoocom--81394934;root--80925013;tweetedtimescom--81013811;econsultancycom--81546865;twittercom--81288281;summifycom--81116519;root--80892856;discovermagazinecom--81299124;nextlevelofnewscom--81063475;root--81113636;techmemecom--80960686;root--80909466;googlecom--81394026;nextmediasourcecom--80932830;root--80918033;techmemecom--80899461;visiblicom--81690853;tweetedtimescom--81097152;twittercom--81560441;root--81087820;';//twittercom--81007762;root--80908191;hootsuitecom--80888598;googlecom--81087486;googlecom--81222959;root--78479223;allthingsdcom--81052453;netnewscheckcom--81088381';

		$chartImg = 'https://chart.googleapis.com/chart?' . http_build_query(array(
								'cht' => 'gv',
								'chl' => 'graph{' . $graphString . '}',
										//'chs' => '750x400'
						));

		echo '<img src="' . $chartImg . '">';
	}

}
