<?php

class Visualization_Flare extends Visualization {

	/*
	 * foreach node in list
	 *		if the node is a child of the parent, add as branch
	 *			branch = getTree(the child,allNodes)
	 */
	public function getTree($parentId,$nodeList,$loopDetection=null)
	{
		if(is_null($loopDetection)) $loopDetection = array();
		$tree = array(
				'id' => $parentId,
				'name' => $parentId,
				'data' => new stdClass(),
				'children' => array()
		);
		//error_log("Starting tree for $parentId");
		foreach($nodeList as $node) {
			$nodeParent = $node['parent_id'];
			$nodeChild = $node['child_id'];
			if ($nodeParent == $parentId && $nodeParent != $nodeChild && !in_array($nodeChild,$loopDetection)) { // ignore loops
				//error_log("Parent of $nodeChild is $nodeParent, creating a subtree");
				$loopDetection[] = $nodeParent;
				$tree['children'][] = $this->getTree($nodeChild,$nodeList,$loopDetection);
			}
		}
		return $tree;
	}

	public function draw()
	{
		// create tree from root node
		$tree = $this->getTree($this->creatorId,$this->map);
		$this->drawTree($tree);
	}

	private function drawTree($tree,$divId='infovis')
	{
		error_log("Tree is: " . print_r($tree,true));
		$width = 950;
		$height = 500;

		?>
<script type="text/javascript" src="/js/protovis-r3.2.js"></script>
<script type="text/javascript">

var tree = <?php echo json_encode($tree); ?>;

var vis = new pv.Panel()
    .width(200)
    .height(2200)
    .left(40)
    .right(160)
    .top(10)
    .bottom(10);

var layout = vis.add(pv.Layout.Cluster)
    .nodes(pv.dom(tree)
        .root("invitation")
        .sort(function(a, b){pv.naturalOrder(a.nodeName, b.nodeName)})
        .nodes())
    .group(true)
    .orient("left");

layout.link.add(pv.Line)
    .strokeStyle("#ccc")
    .lineWidth(1)
    .antialias(false);

layout.node.add(pv.Dot)
    .fillStyle(function(n){n.firstChild ? "#aec7e8" : "#ff7f0e"});

layout.label.add(pv.Label);

vis.render();

</script>
		<?php
	}

}