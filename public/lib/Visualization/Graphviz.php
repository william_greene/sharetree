<?php

require_once 'Image/GraphViz.php';
if (!class_exists('Visualization')) {
	require_once('../Visualization.php');
}

class Visualization_Graphviz extends Visualization {

	public function draw()
	{
		$mapdata = json_encode($this->map);
		echo '<iframe src="/lib/Visualization/Graphviz.php?graphviz_data='.urlencode($mapdata).'" width="1600" height="600"></iframe>';
	}

	public function externalDraw()
	{
		$graph = new Image_GraphViz();

		$knownNodes = array();
		foreach($this->map as $node)
		{
			$parentId = $node['parent_id'];
			$childId = $node['child_id'];

			foreach(array($parentId,$childId) as $id) {
				if (!array_key_exists($id, $knownNodes))
				{
					$graph->addNode(
						$id,
						array(
								'id' => 'n'.$id,
								'shape' => 'box',
								'label' => 'ID: '.$id
						)
					);
				}
			}

			$graph->addEdge(
					array(
							$parentId => $childId
					)
			);
		}

		/*
		$graph->addNode(
						'Node1',
						array(
								'URL' => 'http://link1',
								'label' => 'This is a label',
								'shape' => 'box'
						)
		);

		$graph->addNode(
						'Node2',
						array(
								'URL' => 'http://link2',
								'fontsize' => '14'
						)
		);

		$graph->addNode(
						'Node3',
						array(
								'URL' => 'http://link3',
								'fontsize' => '20'
						)
		);

		$graph->addEdge(
						array(
								'Node1' => 'Node2'
						),
						array(
								'label' => 'Edge Label'
						)
		);

		$graph->addEdge(
						array(
								'Node1' => 'Node2'
						),
						array(
								'color' => 'red'
						)
		);
		 */

		//$graph->image();
		header("Content-Type: application/xml");
		$output = $graph->fetch();
		echo $output;
	}

}

// weirdish: the draw function calls the class independently as an endpoint to render the content of an iframe
if (!empty($_GET['graphviz_data']))
{
	$map = json_decode($_GET['graphviz_data'],true);
	error_log($_GET['graphviz_data']);
	error_log("Graphviz got map: " . print_r($map,true));
	$viz = new Visualization_Graphviz(null,null,$map,null);
	$viz->externalDraw();
}
