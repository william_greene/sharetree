<?php

class Visualization_SpaceTree extends Visualization {

	/*
	 * foreach node in list
	 *		if the node is a child of the parent, add as branch
	 *			branch = getTree(the child,allNodes)
	 */
	public function getTree($parentId,$nodeList,$loopDetection=null)
	{
		if(is_null($loopDetection)) $loopDetection = array();
		$tree = array(
				'id' => $parentId,
				'name' => $parentId,
				'data' => new stdClass(), // appendix needed by Spacetree
				'children' => array()
		);
		//error_log("Starting tree for $parentId");
		foreach($nodeList as $index => $node) {
			$nodeParent = $node['parent_id'];
			$nodeChild = $node['child_id'];
			unset($nodeList[$index]);
			if ($nodeParent == $parentId && $nodeParent != $nodeChild && !in_array($nodeChild,$loopDetection)) { // ignore loops
				//error_log("Parent of $nodeChild is $nodeParent, creating a subtree");
				$loopDetection[] = $nodeParent;
				$tree['children'][] = $this->getTree($nodeChild,$nodeList,$loopDetection);
			}
			else
			{
				if ($nodeParent == $nodeChild) {
					error_log("$nodeChild looks like a loop");
				}
				if (in_array($nodeChild,$loopDetection)) {
					error_log("$nodeChild has been used further up the tree");
					error_log(print_r($node,true));
				}
			}
		}
		return $tree;
	}

	public function draw()
	{
		// create tree from root node
		$tree = $this->getTree($this->creatorId,$this->map);
		// use tree to calculate totals per attendee
		$this->setAttendeeTreeTotals($tree);
		//error_log("Attendees: " . print_r($this->attendees,true) );
		// draw tree
		//error_log("Tree: " . print_r($tree,true));
		$this->drawTree($tree);
	}

	// holy shit this attendee shit is confusing. REFACTOR.
	private function setAttendeeTreeTotals($tree)
	{
		$nodeId = $tree['id'];
		$totalChildren = count($tree['children']);
		$totalPvs = $this->attendees[$nodeId]['pv_count'];
		$totalSessions = $this->attendees[$nodeId]['session_count'];
        $totalFollowers = $this->attendees[$nodeId]['follower_count'];
		foreach($tree['children'] as $branch)
		{
			list($subChildren,$subPvs,$subSessions,$subFollowers) = $this->setAttendeeTreeTotals($branch);
			$totalChildren += $subChildren;
			$totalPvs += $subPvs;
			$totalSessions += $subSessions;
            $totalFollowers += $subFollowers;
		}
		$this->attendees[$nodeId]['sub_attendees']['cumulative'] = $totalChildren;
		$this->attendees[$nodeId]['total_pvs'] = $totalPvs;
		$this->attendees[$nodeId]['total_sessions'] = $totalSessions;
        $this->attendees[$nodeId]['total_followers'] = $totalFollowers;

		return array($totalChildren,$totalPvs,$totalSessions,$totalFollowers);
	}

	private function drawTree($tree,$divId='infovis')
	{
		?>
<script type="text/javascript" src="/js/libs/jquery-1.4.4.min.js"></script>
<script type="text/javascript" src="/js/jit-yc.js"></script>
<script type="text/javascript">
	/*
	 * awe.sm/Plancast invitation tree mapping visualization
	 * Copyright (c) 2011 Snowball Factory, Inc. except where noted.
	 * Uses excellent SpaceTree library from JIT (thejit.org)
	 */

	var labelType, useGradients, nativeTextSupport, animate;

	(function() {
		var ua = navigator.userAgent,
		iStuff = ua.match(/iPhone/i) || ua.match(/iPad/i),
		typeOfCanvas = typeof HTMLCanvasElement,
		nativeCanvasSupport = (typeOfCanvas == 'object' || typeOfCanvas == 'function'),
		textSupport = nativeCanvasSupport
			&& (typeof document.createElement('canvas').getContext('2d').fillText == 'function');
		//I'm setting this based on the fact that ExCanvas provides text support for IE
		//and that as of today iPhone/iPad current text support is lame
		labelType = (!nativeCanvasSupport || (textSupport && !iStuff))? 'Native' : 'HTML';
		nativeTextSupport = labelType == 'Native';
		useGradients = nativeCanvasSupport;
		animate = !(iStuff || !nativeCanvasSupport);
	})();

	/*
	 * JavaScript Pretty Date
	 * Originally copyright (c) 2008 John Resig (jquery.com)
	 * Extended to handle dates > 1 month ago, and fixed bug at 1 week.
	 * Licensed under MIT license
	 */
	// Takes an ISO time and returns a string representing how
	// long ago the date represents.
	function prettyDate(time){
		var date = new Date((time || "").replace(/-/g,"/").replace(/[TZ]/g," ")),
			diff = (((new Date()).getTime() - date.getTime()) / 1000),
			day_diff = Math.floor(diff / 86400);

		if ( isNaN(day_diff) || day_diff < 0 )
			return;

		return day_diff == 0 && (
				diff < 60 && "just now" ||
				diff < 120 && "1 minute ago" ||
				diff < 3600 && Math.floor( diff / 60 ) + " minutes ago" ||
				diff < 7200 && "1 hour ago" ||
				diff < 86400 && Math.floor( diff / 3600 ) + " hours ago") ||
			day_diff == 1 && "Yesterday" ||
			day_diff <= 7 && day_diff + " days ago" ||
			day_diff <= 60 && Math.ceil( day_diff / 7 ) + " weeks ago" ||
			"more than 2 months ago";
	}

	var attendees = <?php echo json_encode($this->attendees); ?>;
	var plan = <?php echo json_encode($this->extra); ?>;
	
	var selectedId = false;
	
	function init<?php echo $divId; ?>(){

		//init data
		var json = <?php echo json_encode($tree); ?>;
		
		//init Spacetree
		//Create a new ST instance
		var st = new $jit.ST({
				//id of viz container element
				injectInto: '<?php echo $divId; ?>',
				//set duration for the animation
				duration: 300,
				//set animation transition type
				transition: $jit.Trans.Quart.easeInOut,
				//set distance between node and its children
				levelDistance: 120,
				// show full tree from the start?
				constrained: true,
				// how deep can tree be?
				levelsToShow: 1,
				//enable panning
				Navigation: {
					enable:true,
					panning:true
				},
				// nodes go top-down or left-right
				//orientation: 'left',
				orientation: 'top',
				// alignment
				alignment: 'right',
				// how far from the center should the node be
				offsetY: 0,
				offsetX: 0,
				//set node and edge styles
				//set overridable=true for styling individual
				//nodes or edges
				Node: {
					height: 50,
					width: 60,
					type: 'rectangle',
					color: 'transparent',
					overridable: true
				},

				Edge: {
					type: 'bezier',
					overridable: true
				},

				onBeforeCompute: function(node){
					//Log.write("loading " + node.name);
				},

				onAfterCompute: function(){
					//Log.write("done");
				},

				//This method is called on DOM label creation.
				//Use this method to add event handlers and styles to
				//your node.
				onCreateLabel: function(label, node){
					label.id = node.id;
					// create our visual node (which is technically an HTML label)
					label.innerHTML = makeNode(node);
					label.onclick = function(){
						// re-center the graph
						st.onClick(node.id, { Move: {
								enable: true,
								offsetX: st.canvas.translateOffsetX,
								offsetY: st.canvas.translateOffsetY
						} });
						// make info visible, hide previous one
						if (selectedId) {
							$('#'+selectedId).removeClass('selected');
						}
						var infoId = 'info-'+label.id.replace('.','-');
						$('#'+infoId).addClass('selected');
						selectedId = infoId;
					};
					// attach a mouseover handler to the content
					/*
					switch (attendees[node.id]['type'])
					{
						case 'rootNode':
							break;
						default:
							label.childNodes.item(0).childNodes.item(0).onmouseover = function() {
								showToolTip(node,label);
							}
					}
					*/
				},

				//This method is called right before plotting
				//a node. It's useful for changing an individual node
				//style properties before plotting it.
				//The data properties prefixed with a dollar
				//sign will override the global node style properties.
				onBeforePlotNode: function(node){
					//add some color to the nodes in the path between the
					//root node and the selected node.
					//node.onclick = function() { alert('hi'); };
					if (node.selected) {
						//node.data.$color = "#ff7";
					}
					else {
						delete node.data.$color;
						//if the node belongs to the last plotted level
						if(!node.anySubnode("exist")) {
							//count children number
							var count = 0;
							node.eachSubnode(function(n) { count++; });
							//assign a node color based on
							//how many children it has
							//node.data.$color = ['#aaa', '#baa', '#caa', '#daa', '#eaa', '#faa'][count];
						}
					}
				},

				//This method is called right before plotting
				//an edge. It's useful for changing an individual edge
				//style properties before plotting it.
				//Edge data proprties prefixed with a dollar sign will
				//override the Edge global style properties.
				onBeforePlotLine: function(adj){
					adj.data.$lineWidth = 2;

					var childNodeId = adj.nodeTo.id;
					var childMeta = attendees[childNodeId];
					if (childMeta['type'] == 'mention') {
						if (childMeta['retweet_or_mention'] == 'mention') {
                            // mention
							adj.data.$color = '#fff';
						} else {
                            // retweet
							adj.data.$color = "#85E4FF";
						}
                    } else if (childMeta['type'] == 'domain' || childMeta['type'] == 'directNode') {
						// top-level nodes
                        adj.data.$color = '#ddd';
					} else {
						// re-shares
                        adj.data.$color = '#3b5997';
					}
				},

				onAfterCompute: function() {
					//st.switchPosition("top","replot");
				}
		});
		//load json data		
		st.loadJSON(json);
		//compute node positions and layout
		st.compute();
		//optional: make a translation of the tree
		//st.geom.translate(new $jit.Complex(-200, 0), "current");
		//emulate a click on the root node.
		st.onClick(st.root);
		//end
	}
	
	//var rootNodeId = plan['attendee']['id'];
	var rootNodeId = '_root';

	var makeNode = function(node) {
		var id = node.id;
		var childrenCode = '';
		var withChildrenClass = '';
		if (typeof attendees[id]['sub_attendees'] !== 'undefined' && attendees[id]['sub_attendees']['cumulative'] > 0 && id != rootNodeId) {
			childrenCode = '<div class="children">'
			childrenCode += '+' + attendees[id]['sub_attendees']['cumulative'];
			childrenCode += '</div>';
			withChildrenClass = 'withChildren';
		}
		var metaNode = attendees[id];
		var html;
		var infoId = 'info-'+id;
		infoId = infoId.replace('.','-');
		switch(metaNode.type)
		{
			case 'rootNode':
				html = '<div class="node2 rootNode"></div>'
				html += '<div class="info rootNode" id="' + infoId +'"><h2><a href="http://blog.awe.sm/2011/07/14/twitter-drives-4-times-as-much-traffic-as-you-think-it-does/">Twitter Drives 4x as much traffic as you think it does</a></h2>'
				//html += '<p>Posted 2011-07-14</div>'
				break;
			case 'directNode':
				html = '<div class="node2 directNode"></div>'
				html += '<div class="info directNode" id="' + infoId +'"><h2>Direct traffic</h2></div>'
				break;
			case 'domain':
				html = '<div class="node2 domain" style="background-image:url('+ metaNode.icon +')"></div>'
				html += '<div class="info domain" id="' + infoId +'">';
				html += '<h2><a href="http://' + metaNode['domain'] + '">' + metaNode['domain'] + '</a>';
				if (metaNode['article_title']) {
					html += ': <a href="' + metaNode['article_url'] + '">' + metaNode['article_title'] + '</a>';
				}
				html += '</h2>';
				html += '<div class="directViews">Direct: ' + metaNode['session_count'] + ' visits, ' + metaNode['pv_count'] + ' pageviews</div>';
				html += '<div class="totalViews">Total: ' + metaNode['total_sessions'] + ' visits, ' + metaNode['total_pvs'] + ' pageviews</div>';
				html += '</div>';
				break;			
			case 'tweet':
				html = '<div class="node2 tweet" style="background-image:url('+ metaNode.icon +')"></div>'
				html += '<div class="info tweet" id="' + infoId +'">';
				html += '<div class="fulltweet"><a target="_blank" href="' + metaNode['tweet_url'] + '">@' + metaNode['username'] + '</a>: <span class="tweetbody">' + metaNode['tweet_body'] + '</span></div>';
				html += '<div class="date">'+ metaNode['date'] +' via '+ metaNode['tool'] +'</div>';
				html += '<div class="mentions">'+ (metaNode['mentions']-1) +' mentions</div>'
                //html += '<br><div class="followers">'+ metaNode['follower_count'] +' followers, '+ metaNode['total_followers'] +' total</div>'
                html += '<br><div class="followers">'+ metaNode['follower_count'] +' followers</div>'
				html += '<div class="directViews">Direct: ' + metaNode['session_count'] + ' visits, ' + metaNode['pv_count'] + ' pageviews</div>';
				html += '<div class="totalViews">Total: ' + metaNode['total_sessions'] + ' visits, ' + metaNode['total_pvs'] + ' pageviews</div>';
				html += '</div>'
				break;
			case 'mention':
				html = '<div class="node2 mention sub-'+ metaNode['retweet_or_mention'] +'" style="background-image:url('+ metaNode.icon +')"></div>'
				html += '<div class="info mention" id="' + infoId +'">'
				html += '<div class="fulltweet"><a href="' + metaNode['tweet_url'] + '">@' + metaNode['username'] + '</a>: <span class="tweetbody">' + metaNode['tweet_body'] + '</span></div>';
				html += '<div class="followers">'+ metaNode['follower_count'] +' followers</div>'
				html += '<div class="date">'+ metaNode['date'] +' via '+ metaNode['tool'] +'</div>';
				html += '</div>'
				break;
		}
		html += childrenCode;
		return html;
	}

	var previousLabel = null;
	var previousInner = null;
	var previousNode = null;
	var toolTipVisible = false;
	var showToolTip = function(node,label) {

		var id = node.id;
		var attendee = attendees[id];

		hideToolTip(previousNode,previousLabel); // hide previous tooltip if any, if necessary

		// store for reset
		previousLabel = label;
		previousInner = label.innerHTML;
		previousNode = node;
		var code =
			'<div class="infotip">'+
			label.childNodes.item(0).innerHTML;
		
		// how long ago joined/created
		if (id != rootNodeId) {
			if (attendee['sub_attendees']['cumulative'] > 0) {
				code += '<h3>Total:</h3>';
				code += '<div class="infoline pvs">Page views: ' + attendee['total_pvs'] + '</div>';
				code += '<div class="infoline sessions">Visits: ' + attendee['total_sessions'] + '</div>';
			}
			code += '<h3>Direct:</h3>';
			code += '<div class="infoline pvs">Page views: ' + attendee['pv_count'] + '</div>';
			code += '<div class="infoline sessions">Visits: ' + attendee['session_count'] + '</div>';
			if (typeof attendee['mention_count'] != 'undefined' && attendee['mention_count'] != null) {
				code += '<div class="infoline avatar"><img src="' + attendee['avatar'] + '"></div>';
				code += '<div class="infoline mentions">Mentions: ' + attendee['mention_count'] + '</div>';
				code += '<div class="infoline awesm_url">Link: ' + attendee['awesm_url'] + '</div>';
			}
			
		}

		
		code += '</div>';
		label.innerHTML = code;

		// turn username into a link
		var usernameLink = label.childNodes.item(0).childNodes.item(1).childNodes.item(0);
		if(usernameLink && node.id != '__others') {
			console.log(attendees[id]);
			usernameLink.innerHTML = '<a target="_blank" href="' + attendees[id]['url'] + '">' + attendees[id]['best_name'] + '</a>';
		}
		// hide tooltip when we mouseout of dark rectangle
		label.childNodes.item(0).onmouseout = function(e) {
			if (e.toElement.tagName == 'A') return; // it thinks the link is mousing out for some reason.
			if (e.currentTarget != e.target) return; // its definition of "out" is weird
			hideToolTip(node,label);
		};
		// if we mouse out of bubble, hide if we're moving to the canvas, but otherwise keep it showing
		label.onmouseout = function(e) {
		   if (e.toElement.tagName == 'CANVAS') {
			   hideToolTip(node,label);
		   }
			
		}
		toolTipVisible = true;
	}
	// hides a tool tip if there is one
	var hideToolTip = function(node,label) {
		if(toolTipVisible) {
			// reset to the previous HTML
			label.innerHTML = previousInner;
			// reapply the previous mouseover listener
			label.childNodes.item(0).childNodes.item(0).onmouseover = function() {
				showToolTip(node,label);
			}
			toolTipVisible = false;
		}
	}

</script>

<div id="<?php echo $divId; ?>"></div>
<script>window.addEventListener('load',init<?php echo $divId; ?>,false);</script>

		<?php
	}

}