<?php

function neat_trim($str, $n, $delim='...') {
   $len = strlen($str);
   if ($len > $n) {
       preg_match('/(.{' . $n . '}.*?)\b/', $str, $matches);
       return rtrim($matches[1]) . $delim;
   }
   else {
       return $str;
   }
}

function getPlan($planId,$bWithAttendees=false)
{
	$planApiBase = "http://api.plancast.com/02/plans/show.json?plan_id=%s";
	if($bWithAttendees) $planApiBase .= '&extensions=attendees,awesm';
	$planApiCall = sprintf($planApiBase, $planId);
	error_log("Curling $planApiCall");
	$ch = curl_init($planApiCall);
	curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
	curl_setopt($ch, CURLOPT_HEADER, false);
	$response = curl_exec($ch);
	$plan = json_decode($response, true);
	return $plan;
}