<?php

$dbParams = array(
	'host' => '10.0.1.47',
	'dbname' => 'explorepvs',
	'username' => 'seldo',
	'password' => 'seldo'
);

$pdo = new PDO(
    "mysql:host={$dbParams['host']};dbname={$dbParams['dbname']}",
    $dbParams['username'],
    $dbParams['password']
);

// get the relations
$sql = "select * from session_tree";
$nodeList = $pdo->query($sql)->fetchAll(PDO::FETCH_ASSOC);

// get redirection metadata
$sql = "select * from redirections";
$redirections = $pdo->query($sql)->fetchAll(PDO::FETCH_ASSOC);
$redirectionsIndexed = array();
$awesmUrlMap = array();
foreach($redirections as $r)
{
	$redirectionsIndexed[$r['id']] = $r;
	$awesmUrl = $r['domain'].'_'.$r['stub'];
	$awesmUrlMap[$awesmUrl] = $r['id'];
}

// get mentions metadata per-awesmurl
$row = 1;
if (($handle = fopen("mentions.csv", "r")) !== FALSE) {
    while (($data = fgetcsv($handle, 1000, ",")) !== FALSE) {
        error_log(print_r($data,true));
		$awesmUrl = $data[0];
		$redirectionId = $awesmUrlMap[$awesmUrl];
		if (!empty($redirectionId)) {
			$redirectionsIndexed[$redirectionId]['mention_count'] = $data[1];
			$redirectionsIndexed[$redirectionId]['twitter_username'] = $data[3];
			$redirectionsIndexed[$redirectionId]['avatar'] = $data[5];
			$redirectionsIndexed[$redirectionId]['mention_url'] = $data[6];
		}
    }
    fclose($handle);
}

// create node metadata list
$nodeInfo = array();
foreach($nodeList as $node)
{
	$nodeId = $node['id_string'];
	if (empty($nodeId)) $nodeId = '_blank';
	if (substr($node['type'],0,11) == 'redirection') {
		$r = $redirectionsIndexed[$nodeId];
		$node['awesm_url'] = $r['domain'].'/'.$r['stub'];
		if (!empty($r['twitter_username'])) {
			//$node['best_name'] = '<a href="http://twitter.com/'.$r['twitter_username'].'">@'.$r['twitter_username'].'</a>';
			//$node['best_name'] = '<a target="_blank" href="'.$r['mention_url'].'">@'.$r['twitter_username'].'</a>';
			$node['best_name'] = '@'.$r['twitter_username'];
			$node['url'] = $r['mention_url'];
		} else {
			$node['best_name'] = $node['awesm_url'];
			$node['url'] = 'http://'.$node['awesm_url'];
		}
		
		$node['mention_count'] = $r['mention_count'];
		$node['avatar'] = $r['avatar'];
		$node['mention_url'] = $r['mention_url'];
		$node['twitter_username'] = $r['twitter_username'];
	} else {
		$node['best_name'] = $node['label'];		
		$node['url'] = 'http://'.$node['label'];
	}
	$nodeInfo[$nodeId] = $node;
}
$nodeInfo['_root'] = array(
	'best_name' => "All Referrers"
);
$nodeInfo['_blank']['best_name'] = 'Direct';

// turn it into a map
$map = array();
$count = 0;
foreach($nodeList as $node) {
	$childId = $node['id_string'];
	if (empty($childId)) {
		$childId = '_blank';
	}
	
	$parentId = $node['parent_string'];
	if (is_null($parentId)) {
		$parentId = '_root';
	} else if (empty($parentId)) {
		$parentId = '_blank';
	}
	
	if ($bSimpleNames) {
		$parentId = str_replace('.','',$parentId);
		$parentId = str_replace('-','',$parentId);
		$childId = str_replace('.','',$childId);
		$childId = str_replace('-','',$childId);
	}
	
	$map[] = array(
		'parent_id' => $parentId,
		'child_id' => $childId
	);
	$count++;
	if ($count > $limit) break;
}
