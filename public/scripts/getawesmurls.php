<?php

// get the shares
$dbParams = array(
	'host' => '10.0.1.47',
	'dbname' => 'explorepvs',
	'username' => 'seldo',
	'password' => 'seldo'
);

$pdo = new PDO(
    "mysql:host={$dbParams['host']};dbname={$dbParams['dbname']}",
    $dbParams['username'],
    $dbParams['password']
);

// get redirection metadata
$sql = "select * from redirections";
$redirections = $pdo->query($sql)->fetchAll(PDO::FETCH_ASSOC);
$redirectionsIndexed = array();
$awesmUrlMap = array();
foreach($redirections as $r)
{
	$redirectionsIndexed[$r['id']] = $r;
	$awesmUrl = $r['domain'].'_'.$r['stub'];
	$awesmUrlMap[$awesmUrl] = $r['id'];
}

// get the pageview counts driven by each awesm_url
$sql = "select concat(r.domain,'_',r.stub) as awesm_url, s.redirection_id, sum(s.pageview_count) as pvs, count(*) as sessions 
from sessioncountsonepost s, redirections r where s.redirection_id != 0 and s.redirection_id = r.id group by redirection_id";
$awesmCounts = $pdo->query($sql)->fetchAll(PDO::FETCH_ASSOC);

// munge them with the redirection metadata including parents
$awesmUrls = array();
foreach($awesmCounts as $count)
{
	$awesmUrl = $count['awesm_url'];
	$awesmUrlMetadata = $redirectionsIndexed[$count['redirection_id']];
	$parentAwesm = $awesmUrlMetadata['parent_awesm'];
	// if the share that drove these views does not itself have a parent
	if (empty($parentAwesm)) {
		// then the parent becomes the referring domain of this share
		// the referring domain of this share is the referring domain of the 
		// first PV with a clicker_id equal to the snowball_id of this share
		$snowballId = $awesmUrlMetadata['snowball_id'];
		$sql = "select seldo_get_domain(referrer) as domain from pvsonepost where clicker_id = '$snowballId' and seldo_get_domain(referrer) != 'blog.awe.sm' order by converted_at asc";
		$firstPV = $pdo->query($sql)->fetch(PDO::FETCH_ASSOC);
		if ($firstPV) {
			$parentAwesm = $firstPV['domain'];
		} else {
			$parentAwesm = '_blank';
		}
	}
	$awesmUrls[$awesmUrl] = array(
		'type' => 'tweet',
		'pv_count' => $count['pvs'],
		'session_count' => $count['sessions'],
		'awesm_url' => $count['awesm_url'],
		'parent_id' => $parentAwesm
	);
}

echo json_encode($awesmUrls);