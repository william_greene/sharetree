<?php

$domains = array();

// get referring domains data, output as JSON
$row = 1;
if (($handle = fopen("../data/referring-domains.csv", "r")) !== FALSE) {
    while (($data = fgetcsv($handle, 1000, ",")) !== FALSE) {
        $domain = $data[0];
		$pvs = $data[1];
		$sessions = $data[2];
		$domains[$domain] = array(
			'type' => 'domain',
			'domain' => $domain,
			'pv_count' => $pvs,
			'session_count' => $sessions
		);
    }
    fclose($handle);
}

echo json_encode($domains);