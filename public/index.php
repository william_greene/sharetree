<?php
require('controller.inc');
?><!doctype html>
<head>
	<meta charset="utf-8">
	<title>Share tree</title>
	<link rel="stylesheet" href="/css/<?php echo $vizMethod; ?>.css">
</head>

<div id="main" role="main">
	<?php
		if (!empty($map))
		{
			$vizClass = "Visualization_$vizMethod";
			$viz = new $vizClass($rootId,$nodeInfo,$map,null,null);
			$viz->draw();
		}
	?>
</div>

</body>
</html>