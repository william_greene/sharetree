<?php

require('lib/util.php');
require('lib/awesm.class.php');
require('lib/Visualization.php');
require('lib/Visualization/Google.php');
require('lib/Visualization/Graphviz.php');
require('lib/Visualization/Spacetree.php');
require('lib/Visualization/Flare.php');
require('lib/Visualization/RGraph.php');

$vizMethod = 'SpaceTree';
if (!empty($_REQUEST['viz'])) {
	$vizMethod = $_REQUEST['viz'];
}

$dataSource = 'csv';
if (!empty($_REQUEST['data'])) {
	$dataSource = $_REQUEST['data'];
}

switch($vizMethod)
{
	case 'Google':
		$limit = 91;
		$bSimpleNames = true;
		break;
	case 'Graphviz':
		$limit = 50;
		$bSimpleNames = true;
		break;
	case 'SpaceTree':
		$limit = 1000;
		$bSimpleNames = false;
		break;
	default:
		$bSimpleNames = false;
		$limit = 1000;
}

// get the parent-child data
if ($dataSource == 'canned')
{
	// load from premade JSON
	$nodeInfo = json_decode(file_get_contents('./data/nodeinfo.json'),true);
	$map = json_decode(file_get_contents('./data/map.json'),true);
}
else if ($dataSource == 'live')
{
	// load from DB
	include('loadlive.inc');
	
	$nodeString = json_encode($nodeInfo);
	$mapString = json_encode($map);
	
	file_put_contents('./data/nodeinfo.json', $nodeString);
	file_put_contents('./data/map.json',$mapString);
}
else if ($dataSource == 'test')
{
	include('newtree.inc');
}
else if ($dataSource == 'csv')
{
	include('csvtree.inc');
}
//error_log("Map: " . print_r($map,true));

$rootId = '_root';