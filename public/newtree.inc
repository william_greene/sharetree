<?php

$nodeInfo = array(
	'_root' => array(
		'best_name' => 'Twitter Drives 4x<br> as much traffic<br> as you think',
		'icon' => '/img/screenshot.png',
		'type' => 'rootNode'
	),
	'_blank' => array(
		'best_name' => 'Direct',
		'icon' => '/img/crowd.png',
		'type' => 'directNode'
	),
	'awesm_d1' => array(
		'best_name' => 'Direct Tweet 1',
		'icon' => 'http://a0.twimg.com/profile_images/1322232523/Mushroom_copy2_normal.png',
		'type' => 'tweet',
		
	),
	'awesm_d2' => array(
		'best_name' => 'Direct Tweet 2',
		'icon' => 'http://a1.twimg.com/profile_images/30105312/profile-photo_normal.jpg',
		'type' => 'tweet',
		'pv_count' => 10,
		'session_count' => 5
	),
	'r1' => array(
		'best_name' => "TechMeme",
		'icon' => '/img/favicon-techmeme.gif',
		'type' => 'domain',
		'domain' => 'techmeme.com',
		'article_url' => 'blahblahblah',
		'article_title' => 'Twitter Drives 4x as much traffic as you think',
		'pv_count' => 100,
		'session_count' => 90
	),
	'r1t1' => array(
		'best_name' => "TechMeme Tweet 1",
		'icon' => 'http://a3.twimg.com/profile_images/288574342/me_hat_normal.jpg',
		'type' => 'tweet',
		'username' => 'fishmark',
		'full_name' => 'Mark Fish',
		'tweet_url' => 'http://twitter.com/fishmark/status/91541008109670400',
		'tweet_body' => 'Great info! RT @awesm Guess what: Twitter drives 4 times as much traffic as you think it does! http://t.co/m40pLXI',
		'pv_count' => 200,
		'session_count' => 180,
		'timestamp' => '2011-05-15 5:50:01pm',
		'tool' => 'Tweetdeck',
		'tool_url' => 'http://blah blah'
	),
	'r1t2' => array(
		'best_name' => "TechMeme Tweet 2",
		'icon' => 'http://a3.twimg.com/profile_images/432238522/Photo_on_2009-09-22_at_14.35__3_normal.jpg',
		'type' => 'tweet',
		'username' => 'izs',
		'full_name' => 'isaacs',
		'tweet_url' => 'http://twitter.com/izs/status/91546461002665984',
		'tweet_body' => 'Guess what: Twitter drives 4 times as much traffic as you think it does! http://awe.sm/5OqsO /cc @parislemon ;-)',
		'pv_count' => 500,
		'session_count' => 400,
		'timestamp' => '2011-05-15 5:50:01pm',
		'tool' => 'Tweetdeck',
		'tool_url' => 'http://blah blah'		
	),
	'r1t2m1' => array(
		'best_name' => "TechMeme Tweet 2 Mention 1",
		'icon' => 'http://a2.twimg.com/profile_images/35546532/jl-profile_normal.jpg',
		'type' => 'mention',
		'username' => 'izs',
		'full_name' => 'isaacs',
		'tweet_url' => 'http://twitter.com/izs/status/91546461002665984',
		'tweet_body' => 'Guess what: Twitter drives 4 times as much traffic as you think it does! http://awe.sm/5OqsO /cc @parislemon ;-)',
		'pv_count' => 500,
		'session_count' => 400,
		'timestamp' => '2011-05-15 5:50:01pm',
		'tool' => 'Tweetdeck',
		'tool_url' => 'http://blah blah',
		'native_or_mention' => 'native'
	),
	'r1t2m2' => array(
		'best_name' => "TechMeme Tweet 2 Mention 2",
		'icon' => 'http://a3.twimg.com/profile_images/1353645734/ac_normal.jpg',
		'type' => 'mention'
	),
	'r1t2m3' => array(
		'best_name' => "TechMeme Tweet 2 Mention 3",
		'icon' => 'http://a1.twimg.com/profile_images/277013652/2882ab2_normal.jpg',
		'type' => 'mention'
	),
	'r1t2m4' => array(
		'best_name' => "TechMeme Tweet 2 Mention 4",
		'icon' => 'http://a3.twimg.com/profile_images/288574342/me_hat_normal.jpg',
		'type' => 'mention'
	),
	'r1t2m5' => array(
		'best_name' => "TechMeme Tweet 2 Mention 5",
		'icon' => 'http://a3.twimg.com/profile_images/432238522/Photo_on_2009-09-22_at_14.35__3_normal.jpg',
		'type' => 'mention'
	),
	'r1t2m6' => array(
		'best_name' => "TechMeme Tweet 2 Mention 6",
		'icon' => 'http://a2.twimg.com/profile_images/1170183870/jec-fall-2010_normal.jpg',
		'type' => 'mention'
	),
	'r1t2m7' => array(
		'best_name' => "TechMeme Tweet 2 Mention 7",
		'icon' => 'http://a1.twimg.com/sticky/default_profile_images/default_profile_4_normal.png',
		'type' => 'mention'
	),
	'r1t2m8' => array(
		'best_name' => "TechMeme Tweet 2 Mention 8",
		'icon' => 'http://a3.twimg.com/profile_images/1158995009/ldm_normal.JPG',
		'type' => 'mention'
	),
	'r1t2m9' => array(
		'best_name' => "TechMeme Tweet 2 Mention 9",
		'icon' => 'http://a3.twimg.com/profile_images/1202096248/twitterAvatarTech2_normal.png',
		'type' => 'mention'
	),
	
	'r1t2t1' => array(
		'best_name' => "Sub Tweet 1",
		'icon' => 'http://a2.twimg.com/profile_images/1017650397/amir_profile_photo_normal.jpg',
		'type' => 'tweet'		
	),
	'r1t2t2' => array(
		'best_name' => "Sub Tweet 2",
		'icon' => 'http://a3.twimg.com/profile_images/190356508/awesm_Logo_normal.png',
		'type' => 'tweet'		
	),	
	
	'r2' => array(
		'best_name' => "TechCrunch",
		'icon' => '/img/favicon-techcrunch.png',
		'type' => 'domain'
	),
	'r2t1' => array(
		'best_name' => "TechCrunch Tweet 1",
		'icon' => 'http://a2.twimg.com/profile_images/1017650397/amir_profile_photo_normal.jpg',
		'type' => 'tweet'		
	),
	'r2t2' => array(
		'best_name' => "TechCrunch Tweet 2",
		'icon' => 'http://a3.twimg.com/profile_images/190356508/awesm_Logo_normal.png',
		'type' => 'tweet'		
	),
	
);

$map = array(
	array(	'parent_id' => '_root',		'child_id' => '_blank' ), // direct
	array(	'parent_id' => '_blank',	'child_id' => 'awesm_d1' ),
	array(	'parent_id' => '_blank',	'child_id' => 'awesm_d2' ),
	array(	'parent_id' => '_root',		'child_id' => 'r1' ), // techmeme
	array(	'parent_id' => 'r1',		'child_id' => 'r1t1' ),
	array(	'parent_id' => 'r1',		'child_id' => 'r1t2' ),
	array(	'parent_id' => 'r1t2',		'child_id' => 'r1t2m1' ),
	array(	'parent_id' => 'r1t2',		'child_id' => 'r1t2m2' ),
	array(	'parent_id' => 'r1t2',		'child_id' => 'r1t2m3' ),
	array(	'parent_id' => 'r1t2',		'child_id' => 'r1t2m4' ),
	array(	'parent_id' => 'r1t2',		'child_id' => 'r1t2m5' ),
	array(	'parent_id' => 'r1t2',		'child_id' => 'r1t2m6' ),
	array(	'parent_id' => 'r1t2',		'child_id' => 'r1t2m7' ),
	array(	'parent_id' => 'r1t2',		'child_id' => 'r1t2m8' ),
	array(	'parent_id' => 'r1t2',		'child_id' => 'r1t2m9' ),
	array(	'parent_id' => 'r1t2',		'child_id' => 'r1t2t1' ),
	array(	'parent_id' => 'r1t2',		'child_id' => 'r1t2t2' ),
	array(	'parent_id' => '_root',		'child_id' => 'r2' ), // techcrunch
	array(	'parent_id' => 'r2',		'child_id' => 'r2t1' ),
	array(	'parent_id' => 'r2',		'child_id' => 'r2t2' ),
);