<?php

function getTree($parentId,$nodeList,$loopDetection=null)
{
	if(is_null($loopDetection)) $loopDetection = array();
	$tree = array(
			'id' => $parentId,
			'children' => array()
	);
	//error_log("Starting tree for $parentId");
	foreach($nodeList as $index => $node) {
		$nodeParent = $node['parent_id'];
		$nodeChild = $node['child_id'];
		if ($nodeParent == $parentId && $nodeParent != $nodeChild && !in_array($nodeChild,$loopDetection)) { // ignore loops
			//error_log("Parent of $nodeChild is $nodeParent, creating a subtree");
			$loopDetection[] = $nodeParent;
			list($subtree,$subLoop) = getTree($nodeChild,$nodeList,$loopDetection);
			$tree['children'][] = $subtree;
			array_merge($loopDetection,$subLoop);
		}
	}
	return array($tree,$loopDetection);
}

$parentId = '_root';
$nodeList = array(
	array(
		'parent_id' => '_root',
		'child_id' => 'a'
	),
	array(
		'parent_id' => '_root',
		'child_id' => 'b'
	),
	array(
		'parent_id' => 'a',
		'child_id' => 'c'
	)
);

list($tree,$usedNodes) = getTree($parentId,$nodeList);
print_r($tree);
print_r($usedNodes);